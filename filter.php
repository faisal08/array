<?php
function even($var)
{
    // returns whether the input integer is odd
    return!($var & 1);
}

    $array1 = array("a" => 1, "b" => 2, "c" => 3, "d" => 4, "e" => 5);
    print_r(array_filter($array1, "even"));
echo "<br>";

$array1 = array("color" => "red", 2, 4);
$array2 = array("a", "b", "color" => "green", "shape" => "trapezoid", 4);
$result = array_merge($array1, $array2);
print_r($result);
?>